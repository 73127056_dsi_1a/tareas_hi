# tareas del curso de herramientas informaticas

## Estudiante: Jhoseph Anthony Calisaya Vargas

[* Tarea 01](tareas/14.Orden+de+Datos.xls)
#### Tareas Para Casa
- [Ejercicio 1](<Tareas/Ejercicio 01_VENTAS.xlsx>)
- [Ejercicio 2](<Tareas/Ejercicio 02_FACTURA.xlsx>)
- [Ejercicio 3](<Tareas/Ejercicio 03_MOVIMIENTOS.xlsx>)
- [Ejercicio 4](<Tareas/Ejercicio 04_VENTAS-GRAFICOS.xlsx>)
- [Ejercicio 5](<Tareas/Ejercicio 05_LIQUIDACIÓN.xlsx>)
#### Practicas del laboratorio
- [Practica 4 "Trabajo con Celdas Filas Columna"](<Practicas del laboratorio/4.Trabajo+con+Celdas+Filas+Columna.xlsx>)
- [Practica 5 "Formato de Textos"](<Practicas del laboratorio/5. Formato+de+Textos.xlsx>)
- [Practica 6 "Formato Numericos"](<Practicas del laboratorio/6.Formato+Numericos.xlsx>)
- [Practica 7 "Formatos de Tablas y Estilos de Celdas"](<Practicas del laboratorio/7.Formatos+de+Tablas+y+Estilos+de+Celdas.xlsx>)
- [Practica 8 "Formulas Basicas"](<Practicas del laboratorio/8. Formulas+Basicas.xlsx>)
- [Practica 9 "Funciones Basicas"](<Practicas del laboratorio/9. Funciones+Basicas.xlsx>)
- [Practica 10 "Formulas y Funciones con Fechas"](<Practicas del laboratorio/10. Formulas+y+Funciones+con+Fechas.xlsx>)
- [Practica 11 "Graficos de una Variable"](<Practicas del laboratorio/11.Graficos+de+una+Variable.xlsx>)
- [Practica 12 "Graficos de dos o mas variables"](<Practicas del laboratorio/12.Graficos+de+dos+o+mas+variables.xlsx>)
- [Practica 13 "Teoria Concepto Base Datos"](<Practicas del laboratorio/13.Teoria+Concepto+Base+Datos.xlsx>)
- [Practica 14 "Orden de Datos"](<Practicas del laboratorio/14.Orden+de+Datos.xlsx>)
- [Practica 15 "Filtro de Datos"](<Practicas del laboratorio/15.Filtro+de+Datos.xlsx>)
- [Practica 16 "Teoria de las Macros"](<Practicas del laboratorio/16.Teoria+de+las+Macros.xlsm>)
- [Practica 17 "Macros con Graficos"](<Practicas del laboratorio/17.Macros+con+Graficos.xlsm>)
- [Practica 18 "Macro Limpiar"](<Practicas del laboratorio/Limpiar.xlsm>)
- [Practica 19 "Macro Sumar"](<Practicas del laboratorio/Sumar.xlsx>)
- [Practica 20 "Macro Aumentar50"](<Practicas del laboratorio/Aumentar50.xlsx>)
- [Practica final "Empleados"](<Practicas del laboratorio/Empleados.xlsx>)